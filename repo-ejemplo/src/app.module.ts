import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {PadreModule} from './padre/padre.module';
import {HijoModule} from './hijo/hijo.module';
import {TypeOrmModule} from "@nestjs/typeorm";
import {CONFIG_MYSQL} from "./constantes/db-config";

@Module({
    imports: [
        PadreModule,
        HijoModule,
        TypeOrmModule.forRoot(CONFIG_MYSQL),
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {
}
