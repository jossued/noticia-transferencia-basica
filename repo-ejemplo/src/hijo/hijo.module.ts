import {Module} from '@nestjs/common';
import {HijoController} from './hijo.controller';
import {HijoService} from './hijo.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {HijoEntity} from "./hijo.entity";

@Module({
    imports: [
        TypeOrmModule.forFeature([HijoEntity], 'default')],
    controllers: [HijoController],
    providers: [HijoService],
    exports: [HijoService]
})
export class HijoModule {
}
