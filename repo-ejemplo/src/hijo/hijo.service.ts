import { Injectable } from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {Repository} from "typeorm";
import {HijoEntity} from "./hijo.entity";

@Injectable()
export class HijoService {
    constructor(
        @InjectRepository(HijoEntity)
                private readonly _papaRepository: Repository<HijoEntity>) {
    }
}
