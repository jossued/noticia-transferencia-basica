import {Body, Controller, Get, Post} from '@nestjs/common';
import {PadreEntity} from "./padre.entity";
import {HijoEntity} from "../hijo/hijo.entity";
import {PadreService} from "./padre.service";

@Controller('padre')
export class PadreController {

    constructor(
        private readonly _padreService: PadreService,
    ) {
    }

    @Post('crear-padre-hijos')
    private async crearPadreHijos(
        @Body('padre') padre: PadreEntity,
        @Body('hijos') hijos: HijoEntity [],
    ) {
        return await this._padreService.registrarPadreConHijosTransaccion(
            padre,
            hijos
        )
    }

    @Get('')
    responder () {
        return 'Hola'
    }
}
