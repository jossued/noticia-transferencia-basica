import {HttpStatus, Injectable, InternalServerErrorException} from '@nestjs/common';
import {HijoEntity} from "../hijo/hijo.entity";
import {EntityManager, getManager, Repository} from "typeorm";
import {PadreEntity} from "./padre.entity";
import {InjectRepository} from "@nestjs/typeorm";

@Injectable()
export class PadreService {

    constructor(
        @InjectRepository(PadreEntity)
                private readonly _padreRepository: Repository<PadreEntity>) {
    }

    async registrarPadreConHijosTransaccion (padre: PadreEntity, hijos: HijoEntity[])
        : Promise<{mensaje: string, error: boolean, statusCode: number}> {
        return getManager('default')
            .transaction(
                async (transaccionManager: EntityManager) => {
                    try{
                        const padreRepository = transaccionManager.getRepository(PadreEntity);
                        const hijoRepository = transaccionManager.getRepository(HijoEntity);
                        const padreCreado = await padreRepository.save(padre);
                        const hijosDelPadre = hijos.map(
                            (hijoParametro: HijoEntity) => {
                                hijoParametro.padre = padreCreado;
                                return hijoParametro;
                            }
                        );
                        await hijoRepository.save(hijosDelPadre);
                        return {
                            mensaje: 'Padre e hijos creados correctamente',
                            error: false,
                            statusCode: HttpStatus.CREATED,
                        }
                    } catch (error) {
                        console.error(
                            {
                                error,
                                mensaje: 'Error al crear padre e hijos',
                                data: {
                                    padre,
                                    hijos
                                },
                            },
                        );
                        throw new InternalServerErrorException({
                            mensaje: 'Error del servidor',
                        });
                    }
                }
            )
    }
}
