import {Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {HijoEntity} from "../hijo/hijo.entity";

@Entity('padre')
export class PadreEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column(
        {
            type: 'varchar',
            name: 'nombre',
            length: 80
        }
    )
    nombre: string = null;

    @OneToMany(
        type => HijoEntity,
        hijo => hijo.padre
    )
    hijos: HijoEntity [];

}