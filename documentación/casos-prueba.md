## Casos de Prueba
### Crear padre e hijos correctamente
  
  En Postman:

  <img src="/uploads/e395c4167e4c4f84f5eaabb40015ebb5/image.png" width="500"/>

En Base de Datos se comprueba que se hayan guardado:

<img src="/uploads/ca15e37c1a9b9d2be5ce997836a341b1/image.png" width="300"/>

### Crea padre, pero los hijos son null

En Postman:

<img src="/uploads/c73eb2b3ca83b539d68d51928b20bee4/image.png" width="400"/>

En Base de Datos se comprueba que no se hayan guardado:

<img src="/uploads/6157dba3d626041b50bf87e0cca18f75/image.png" width="300"/>
<img src="/uploads/8646042deb48252281cd0c4f4b642f14/image.png" width="300"/>

En el servidor se da el error:
```bash
{
 {
    message: "ER_BAD_NULL_ERROR: Column 'nombre' cannot be null",
    code: 'ER_BAD_NULL_ERROR',
    errno: 1048,
    sqlMessage: "Column 'nombre' cannot be null",
    sqlState: '23000',
    index: 0,
    sql: 'INSERT INTO `hijo`(`id`, `nombre`, `padreId`) VALUES (DEFAULT, NULL, 1)',
    name: 'QueryFailedError',
    query: 'INSERT INTO `hijo`(`id`, `nombre`, `padreId`) VALUES (DEFAULT, ?, ?)',
    parameters: [ null, 1 ]
  },
  mensaje: 'Error al crear padre e hijos',
  data: {
    padre: { nombre: 'Soy tu padre', id: 1 },
    hijos: [ [Object], [Object] ]
  }
}

```
### El caso anterior sin usar el `transaccionEntityManager`:

Código:

```typescript
async registrarPadreConHijosTransaccion (padre: PadreEntity, hijos: HijoEntity[])
        : Promise<{mensaje: string, error: boolean, statusCode: number}> {
        return getManager('default')
            .transaction(
                async (transaccionEntityManager: EntityManager) => {
                    try{
                        // const padreRepository = transaccionEntityManager.getRepository(PadreEntity);
                        // const hijoRepository = transaccionEntityManager.getRepository(HijoEntity);
                        const padreCreado = await this._padreRepository.save(padre);
                        const hijosDelPadre = hijos.map(
                            (hijoParametro: HijoEntity) => {
                                hijoParametro.padre = padreCreado;
                                return hijoParametro;
                            }
                        );
                        await this._hijoRepository.save(hijosDelPadre);
                        return {
                            mensaje: 'Padre e hijos creados correctamente',
                            error: false,
                            statusCode: HttpStatus.CREATED,
                        }
                    } catch (error) {
                        console.error(
                            {
                                error,
                                mensaje: 'Error al crear padre e hijos',
                                data: {
                                    padre,
                                    hijos
                                },
                            },
                        );
                        throw new InternalServerErrorException({
                            mensaje: 'Error del servidor',
                        });
                    }
                }
            )
    }
```


En Postman:

<img src="/uploads/2fd2d1d3ed871649228c4533579c3023/image.png" width="400"/>

En Base de Datos se comprueba que no se hayan guardado:

<img src="/uploads/cc7f9678d02a97760cb03b5438693744/image.png" width="300"/>
<img src="/uploads/0c9cf401efc6f49ec7f02c45ddd4441d/image.png" width="300"/>

_Se comprueba que no se hizo correctamente el rollback_